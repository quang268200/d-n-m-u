<div class=" main-right col-md-3">
  <div class="category">
    <div class="category-title">
      <i class="fa fa-bars"></i> <span>Danh mục sản phẩm</span>
    </div>
    <ul id="test">
      <?php
      include './admin/connect.php';
      $sql_category = "SELECT * FROM category";
      $kq_category = $conn->query($sql_category);
      foreach ($kq_category as $key => $value) {
        ?>
        <li><a href="category.php?id=<?php echo $value['id_category'] ?>">LAPTOP <?php echo $value["name_category"] ?></a></li>
      <?php
      }
      ?>
    </ul>
    <div class="category-search">
      <i style="color: #878787;font-size: 20px;
              font-weight: bold" class="fab fa-sistrix"></i>
      <input type="text" name="" id="category" placeholder="Tìm kiếm danh mục">
    </div>
  </div>
  <script>
    var li = $('#test li');
    var a = $('#test li a');
    $('#category').keyup(function() {
      var nhap = $('#category').val().toUpperCase()
      for (let index = 0; index < li.length; index++) {
        var thu = $(a[index]).text()
        if (thu) {
          txtValue = thu.toUpperCase()
          if (txtValue.indexOf(nhap) > -1) {
            $(li[index]).css('display', 'block')
          } else {
            $(li[index]).css('display', 'none')
          }
        }

      }
    })
  </script>
  <div class="products-xn">
    <div class="category-title">
      <i class="fa fa-bars"></i> <span>Sản phẩm xem nhiều nhất</span>
    </div>
    <ul>
      <?php
      include './admin/connect.php';
      $sql_view = "SELECT * from products ORDER BY view DESC limit 6 ";
      $kq_view = $conn->query($sql_view);
      foreach ($kq_view as $key => $value) {
        ?>
        <li>
          <a href="chitietsp.php?id=<?php echo $value["id_product"] ?>">
            <div class="products-xn-img">
              <img src="./admin/images/<?php echo $value["images"] ?>" alt="">
            </div>
            <h5><?php echo $value["name_product"] ?></h5>
            <p><?php echo number_format($value["sale"]) ?>đ</p>
            <span><del><?php echo number_format($value["price"]) ?>đ</del> <i class="far fa-eye"> &nbsp;<?php echo number_format($value["view"]) ?></i></span>
          </a>
        </li>
      <?php
      }
      ?>
      <!-- <li>
        <a href="#">
          <div class="products-xn-img">
            <img src="./images/Asus1.jpg" alt="">
          </div>
          <h5>Laptop Asus Vivobook S15 S530FN-BQ133T (Gold)</h5>
          <p>17.649.000 đ</p>
          <span><del>15.640.000 đ</del> <i class="far fa-eye">111</i></span>
        </a>
      </li>
      <li>
        <a href="#">
          <div class="products-xn-img">
            <img src="./images/Asus3.jpg" alt="">
          </div>
          <h5>Laptop Asus Vivobook S15 S530FN-BQ133T (Gold)</h5>
          <p>17.649.000 đ</p>
          <span><del>15.640.000 đ</del></span>
        </a>
      </li>
      <li>
        <a href="#">
          <div class="products-xn-img">
            <img src="./images/Asus5.jpg" alt="">
          </div>
          <h5>Laptop Asus Vivobook S15 S530FN-BQ133T (Gold)</h5>
          <p>17.649.000 đ</p>
          <span><del>15.640.000 đ</del></span>
        </a>
      </li>
      <li>
        <a href="#">
          <div class="products-xn-img">
            <img src="./images/Asus7.jpg" alt="">
          </div>
          <h5>Laptop Asus Vivobook S15 S530FN-BQ133T (Gold)</h5>
          <p>17.649.000 đ</p>
          <span><del>15.640.000 đ</del></span>
        </a>
      </li>
      <li>
        <a href="#">
          <div class="products-xn-img">
            <img src="./images/Asus12.jpg" alt="">
          </div>
          <h5>Laptop Asus Vivobook S15 S530FN-BQ133T (Gold)</h5>
          <p>17.649.000 đ</p>
          <span><del>15.640.000 đ</del></span>
        </a>
      </li> -->
    </ul>
  </div>
</div>
<?php session_start() ?>
<?php ob_start() ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chi tiết sản phẩm</title>
    <script src="./js/jquery.js"></script>
  <script src='./js/fontawesome.js'></script>
    <script src="./js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/chitietsp.css">
</head>

<body>
    <?php include './include/header.php' ?>
    <link rel="stylesheet" href="./css/chitietsp.css">
    <div class="main">
        <div class="title">Thông tin sản phẩm</div>
        <div class="row detail">

            <?php
            include './admin/connect.php';
            if (isset($_GET["id"])) {
                $id = $_GET["id"];
                $sql_chitiet = "SELECT * from products where id_product=$id";
                $kq_chitiet = $conn->query($sql_chitiet)->fetch();
            }
            ?>
            <?php
            include './admin/include/function.php';
            if (isset($_SESSION["user"])) {
                $tinhview = view($id);
                $kq_view = $conn->query($tinhview);
            }
            ?>
            <div class="detail-images col-md-4">
                <img src="./admin/images/<?php echo $kq_chitiet["images"] ?>" alt="" width="300px">
            </div>
            <div class="detail-status col-md-8">
                <h3><?php echo $kq_chitiet["name_product"] ?></h3>
                <ul>
                    <li>Giá sản phẩm: <del><?php echo  number_format($kq_chitiet["price"]) ?></del></li>
                    <li>Chỉ còn: <span><?php echo  number_format($kq_chitiet["sale"]) ?></span> </li>
                    <li>Mã sản phẩm: <span>20</span> </li>
                    <li>Số lượng sản phẩm còn lại: <span><?php echo  number_format($kq_chitiet["amount"]) ?></span> sản phẩm</li>
                    <li>Số lượng</li>
                    <form action="" method="post">
                        <input type="text" value="1"><br><br>
                        <input type="submit" value="Mua ngay">
                    </form>
                </ul>
            </div>
        </div>
        <div class="more-information">
            <div class="title-information">
                <p>Thông tin cấu hình chi tiết các phiên bản <span><?php echo $kq_chitiet["name_product"] ?></span>: </p>
            </div>
            <div class="thongtin">
                <p>
                    <?php echo $kq_chitiet["detail"] ?>
                </p>
            </div>
        </div>
        <div class="commnet">
            <div class="title-commnent">Bình luận về sản phẩm</div>
            <?php if (isset($_SESSION["thongbao"])) {
                            echo $_SESSION["thongbao"];
                            unset($_SESSION["thongbao"]);
                        }
                        ?>
            <ul>
                <?php
                $sql_show_comment = "SELECT user.user,user.name,comments.content,comments.create_at, user.avatar,comments.id_comment FROM comments INNER JOIN user ON comments.user= user.user WHERE comments.id_product=$id ";
                $kq_show_comment = $conn->query($sql_show_comment);
                foreach ($kq_show_comment as $key => $value) {
                    ?>
                    <li>
                        <div class="img-user">
                            <img src="./admin/images/<?php echo $value["avatar"] ?>" alt="" width="100%">
                        </div>
                        <?php
                        if(isset($_SESSION["user"])){
                            if ($_SESSION["user"] == $value["user"]) {
                                ?>
                            <a href="./admin/delete/delete_comment_fontend.php?id_comment=<?php echo $value["id_comment"] ?>&&id_product=<?php echo $id ?>" onclick="return confirm('Bạn có chắc chắn muốn xóa không')"><span class="fas fa-times" style="float: right; font-size: 14px;"></span></a>
                        <?php
                            }}
                            ?>
                        <h2><?php echo $value["name"] ?></h2>
                        <span><?php echo $value["create_at"] ?></span>
                        <p><?php echo $value["content"] ?></p>
                    </li>
                  
                <?php
                }
                ?>
            </ul>
            <?php
            if (isset($_SESSION["user"])) {
                ?>
                <div class="comment-send">
                    <form action="" method="post">
                        <textarea name="content" id="" cols="100%" rows="2" placeholder="Nhập bình luận của bạn về sản phẩm" required></textarea><br>
                        <input class="btn btn-danger" type="submit" value="Gửi" name="gui">
                    </form>
                </div>
            <?php
            }
            ?>
            <?php
            if (isset($_POST["gui"])) {
                $content = $_POST["content"];
                $id_product = $id;
                $user = $_SESSION["user"];
                $sql_comment = "INSERT INTO comments values(null,'$content','$id_product','$user',current_timestamp)";
                $kq_comment = $conn->query($sql_comment);
                if ($kq_comment) {
                    header("Refresh:0");
                } else {
                    echo "Lỗi";
                }
            }
            ?>
            <div class="same-products">
                <div class="titel-same-products">
                    <p>Các sản phẩm cùng loại</p>
                </div>
                <ul>
                    <?php
                    $category = $kq_chitiet["id_category"];
                    $show_cungloai = category($id, $category);
                    $kq_cungloai = $conn->query($show_cungloai);
                    foreach ($kq_cungloai as $key => $value) {
                        ?>
                        <li>
                            <a href="chitietsp.php?id=<?php echo $value["id_product"] ?>">
                                <div class="anhsp">
                                    <img src="./admin/images/<?php echo $value["images"] ?>" alt="" width="100%">
                                </div>
                                <h5><?php echo $value["name_product"]  ?></h5>
                                <p><?php echo number_format($value["sale"]) ?>đ</p>
                                <span><del><?php echo number_format($value["price"]) ?>đ</del></span>
                            </a>
                        </li>

                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript">
            var scrolltotop = {
                setting: {
                    startline: 100,
                    scrollto: 0,
                    scrollduration: 1e3,
                    fadeduration: [500, 100]
                },
                controlHTML: '<img src="https://i1155.photobucket.com/albums/p559/scrolltotop/arrow23.png" />',
                controlattrs: {
                    offsetx: 5,
                    offsety: 5
                },
                anchorkeyword: "#top",
                state: {
                    isvisible: !1,
                    shouldvisible: !1
                },
                scrollup: function() {
                    this.cssfixedsupport || this.$control.css({
                        opacity: 0
                    });
                    var t = isNaN(this.setting.scrollto) ? this.setting.scrollto : parseInt(this.setting.scrollto);
                    t = "string" == typeof t && 1 == jQuery("#" + t).length ? jQuery("#" + t).offset().top : 0, this.$body.animate({
                        scrollTop: t
                    }, this.setting.scrollduration)
                },
                keepfixed: function() {
                    var t = jQuery(window),
                        o = t.scrollLeft() + t.width() - this.$control.width() - this.controlattrs.offsetx,
                        s = t.scrollTop() + t.height() - this.$control.height() - this.controlattrs.offsety;
                    this.$control.css({
                        left: o + "px",
                        top: s + "px"
                    })
                },
                togglecontrol: function() {
                    var t = jQuery(window).scrollTop();
                    this.cssfixedsupport || this.keepfixed(), this.state.shouldvisible = t >= this.setting.startline ? !0 : !1, this.state.shouldvisible && !this.state.isvisible ? (this.$control.stop().animate({
                        opacity: 1
                    }, this.setting.fadeduration[0]), this.state.isvisible = !0) : 0 == this.state.shouldvisible && this.state.isvisible && (this.$control.stop().animate({
                        opacity: 0
                    }, this.setting.fadeduration[1]), this.state.isvisible = !1)
                },
                init: function() {
                    jQuery(document).ready(function(t) {
                        var o = scrolltotop,
                            s = document.all;
                        o.cssfixedsupport = !s || s && "CSS1Compat" == document.compatMode && window.XMLHttpRequest, o.$body = t(window.opera ? "CSS1Compat" == document.compatMode ? "html" : "body" : "html,body"), o.$control = t('<div id="topcontrol">' + o.controlHTML + "</div>").css({
                            position: o.cssfixedsupport ? "fixed" : "absolute",
                            bottom: o.controlattrs.offsety,
                            right: o.controlattrs.offsetx,
                            opacity: 0,
                            cursor: "pointer"
                        }).attr({
                            title: "Scroll to Top"
                        }).click(function() {
                            return o.scrollup(), !1
                        }).appendTo("body"), document.all && !window.XMLHttpRequest && "" != o.$control.text() && o.$control.css({
                            width: o.$control.width()
                        }), o.togglecontrol(), t('a[href="' + o.anchorkeyword + '"]').click(function() {
                            return o.scrollup(), !1
                        }), t(window).bind("scroll resize", function(t) {
                            o.togglecontrol()
                        })
                    })
                }
            };
            scrolltotop.init();
        </script>
    </div>
    <?php include './include/footer.php' ?>

</body>

</html>
<?php include './include/include_heder.php' ?>
<?php include './include/include_sidebar.php' ?>
<div class="container">
    <div class="main-right">
        <h2>Thông tin user</h2>
        <?php
        if (isset($_SESSION["thongbao"])) {
            echo $_SESSION["thongbao"];
            unset($_SESSION["thongbao"]);
        }
        ?>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Stt</th>
                    <th>Tài khoản</th>
                    <th>Ảnh</th>
                    <th>Tên</th>
                    <th>Quyền</th>
                    <th>Địa chỉ</th>
                    <th>Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                include './connect.php';
                // start đếm tổng số lượng user
                $sql_count = "SELECT count(user) from user";
                $kq_count = $conn->query($sql_count)->fetchColumn();
                //end đếm tổng số lượng user
                if (!isset($_GET["page"])) {
                    $page = 1;
                } else {
                    $page = $_GET["page"];
                }
                $limit = 5;
                $start = ($page - 1) * $limit;
                $total_page = ceil($kq_count / $limit);
                $sql = "SELECT * from user order by create_at desc limit $start,$limit ";
                $kq = $conn->query($sql);
                $stt = 0;
                foreach ($kq as $key => $value) {
                    $stt=$start+=1;
                    ?>
                    <tr>
                        <td><?php echo $stt ?></td>
                        <td><?php echo $value["user"] ?></td>
                        <td><img width="40px" src="./images/<?php echo $value["avatar"] ?>" alt=""></td>
                        <td><?php echo $value["name"] ?></td>
                        <td><?php if($value["permission"]==0){
                            echo '  <a href="phanquyen.php?id='. $value["user"] .'&&page='.$page.'" class="btn btn-success">User</a>';   
                        }else{
                            echo '  <a href="phanquyen.php?id='. $value["user"] .'&&page='.$page.'" class="btn btn-primary">Admin</a>';  
                        }
                         ?></td>
                        <td><?php echo $value["address"] ?></td>
                        <td style="text-align: center" onclick="return confirm('Bạn có chắc chắn muốn xóa')"><a href="./delete/delete_user.php?id=<?php echo $value['user'] ?>&&page=<?php echo $page ?>"><i class="fa fa-trash-alt"></i> </a></td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <ul class="pagination">
            <div class="phantrang">
                <?php
                if ($page > 1) {
                    ?>
                    <li class="page-item"><a class="page-link" href="show_user.php?page=<?php echo ($page - 1) ?>">Previous</a></li>
                <?php
                }
                ?>

                <?php
                for ($i = 1; $i <= $total_page; $i++) {
                    if($i==$page){
                        echo  '<li class="page-item active"><a class="page-link" >'.$i.'</a></li>';
                    }else{
                echo  '<li class="page-item"><a class="page-link" href="show_user.php?page='.$i.'">'.$i.'</a></li>';
                    }
                }
                ?>
                <?php
                if ($page < $total_page) {
                    ?>
                    <li class="page-item"><a class="page-link" href="show_user.php?page=<?php echo ($page + 1) ?>">Next</a></li>
                <?php
                }
                ?>
            </div>
        </ul>
    </div>

</div>

<div class="clear"></div>

<?php include './include/include_footer.php' ?>
<?php include './include/include_heder.php' ?>
<?php include './include/include_sidebar.php' ?>
<div class="main-right">
    <div class="container-fluid">
        <h3 style="padding-top:20px;">Show danh mục sản phẩm  <a href="add_category.php" class="btn btn-success">Thêm danh mục</a></h3>
        <div class="container">
        <div class="test"><?php if (isset($_SESSION["thongbao"])) {
                    echo $_SESSION["thongbao"];
                    unset($_SESSION["thongbao"]);
                }
                ?></div>
            <table class="table">
                <thead>
                    <tr>
                        <th>ID danh mục</th>
                        <th>Tên danh mục</th>
                        <th>Ngày tạo</th>
                        <th style="text-align: center" >Sửa </th>
                        <th style="text-align: center" >Xóa</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    include './connect.php';
                    $sql = "SELECT * FROM category order by create_at desc";
                    $kq = $conn->query($sql);
                    foreach ($kq as $key => $value) {
                        ?>
                        <tr>
                            <td><?php echo $value["id_category"] ?></td>
                            <td><?php echo $value["name_category"] ?></td>
                            <td><?php echo $value["create_at"] ?></td>
                            <td style="text-align: center" ><a href="./update_category.php?id=<?php echo $value["id_category"]?>"><i class="fa fa-clipboard"></i></a></td>
                            <td style="text-align: center" onclick="return confirm('Bạn có chắc chắn muốn xóa')" ><a href="./delete/delete_category.php?id=<?php echo $value["id_category"]?>"><i class="fa fa-trash-alt"></i> </a></td>
                        </tr>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="clear"></div>
<?php include './include/include_footer.php' ?>
<?php include './include/include_heder.php' ?>
<?php include './include/include_sidebar.php' ?>
<div class="main-right">
    <div class="container-fluid">
        <h3 style="padding-top:20px;">Show comment theo sản phẩm</h3>
        <div class="container">
            <div class="test"><?php if (isset($_SESSION["thongbao"])) {
                                    echo $_SESSION["thongbao"];
                                    unset($_SESSION["thongbao"]);
                                }
                                ?></div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Stt</th>
                        <th>Nội dung</th>
                        <th>Ngày comment</th>
                        <th>Người comment </th>
                        <th style="text-align: center">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    include './connect.php';
                    if (isset($_GET["id"])) {
                        $id = $_GET["id"];
                        $sql_comment = "SELECT * FROM comments where id_product={$id} order by create_at DESC";
                        $kq_comment = $conn->query($sql_comment);
                        $stt=1;
                        foreach ($kq_comment as $key => $value) {
                            ?>
                            <tr>
                                <td><?php echo $stt++ ?></td>
                                <td><?php echo $value["content"] ?></td>
                                <td><?php echo $value["create_at"] ?></td>
                                <td><?php echo $value["user"] ?></td>
                                <td style="text-align:center" onclick="return confirm('Bạn có chắc chắn muốn xóa')"><a href="./delete/delete_comment.php?id_comment=<?php echo $value["id_comment"]?>&&id_product=<?php echo $value["id_product"] ?>"><i class="fa fa-trash-alt"></i> </a></td>
                            </tr>
                    <?php
                        }
                    }
                    ?>

                    <?php
                    // }
                    ?>

                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="clear"></div>
<?php include './include/include_footer.php' ?>
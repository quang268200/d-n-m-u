<?php include './include/include_heder.php' ?>
<?php include './include/include_sidebar.php' ?>
<div class="main-right">
    <div class="container-fluid">
        <h3 style="padding-top:20px;">Show comment sản phẩm</h3>
        <div class="container">
            <div class="test"><?php if (isset($_SESSION["thongbao"])) {
                                    echo $_SESSION["thongbao"];
                                    unset($_SESSION["thongbao"]);
                                }
                                ?></div>
            <table class="table">
                <thead>
                    <tr>
                        <th style="text-align: center;">Stt</th>
                        <th style="text-align: center;">Id sản phẩm</th>
                        <th style="width: 350px;">Tên sản phẩm</th>
                        <th>Ảnh sản phẩm</th>
                        <th style="text-align: center;">Số comment</th>
                        <th>Chi tiết </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    ?>

                    <?php
                    include './connect.php';
                    $limit=4;
                    if(isset($_GET["page"])){
                        $page=$_GET["page"];
                    }else{
                        $page=1;
                    }
                    $start=($page-1)*$limit;
                    $sql_product_comment="SELECT COUNT(id_product) FROM comments GROUP BY id_product";
                    $total_product_comment=$conn->query($sql_product_comment)->rowCount();
                    $total_page=ceil($total_product_comment/$limit);
                    $sql_comments = "SELECT products.id_product, products.name_product,products.images,COUNT(comments.id_product),comments.create_at FROM products INNER JOIN comments ON products.id_product=comments.id_product GROUP BY comments.id_product ORDER BY comments.create_at DESC limit $start,$limit";
                    $kq_comments = $conn->query($sql_comments);
                    $stt = $start+=1;
                    // print_r($kq_comments->fetchAll());
                    // exit();
                    foreach ($kq_comments as $key => $value) {
                        ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $stt++ ?></td>
                            <td style="text-align: center;"><?php echo $value["id_product"] ?></td>
                            <td><?php echo $value["name_product"] ?></td>
                            <td><img src="./images/<?php echo $value['images'] ?>" alt="" width="120px"></td>
                            <td style="text-align: center;"><?php echo $value["COUNT(comments.id_product)"] ?></td>
                            <td><a href="chitietcomment.php?id=<?php echo $value["id_product"] ?>" class="btn btn-success">Chi tiết</a></td>
                        </tr>
                    <?php
                    }
                    ?>


                    <?php
                    ?>
                </tbody>
            </table>
            <ul class="pagination">
                <div class="phantrang">
                    <?php
                    if ($page > 1) {
                        ?>
                        <li class="page-item"><a class="page-link" href="show_comment.php?page=<?php echo ($page - 1) ?>">Previous</a></li>
                    <?php
                    }
                    ?>

                    <?php
                    for ($i = 1; $i <= $total_page; $i++) {
                        if ($i == $page) {
                            echo  '<li class="page-item active"><a class="page-link" >' . $i . '</a></li>';
                        } else {
                            echo  '<li class="page-item"><a class="page-link" href="show_comment.php?page=' . $i . '">' . $i . '</a></li>';
                        }
                    }
                    ?>
                    <?php
                    if ($page < $total_page) {
                        ?>
                        <li class="page-item"><a class="page-link" href="show_comment.php?page=<?php echo ($page + 1) ?>">Next</a></li>
                    <?php
                    }
                    ?>
                </div>
            </ul>
        </div>

    </div>
</div>

<div class="clear"></div>
<?php include './include/include_footer.php' ?>
<?php include './include/include_heder.php'?>
<title>Thêm sản phẩm</title>
<?php include './include/include_sidebar.php' ?>
<div class="main-right">
<?php

include './connect.php';
if (isset($_POST["luu"])) {
    $category = $_POST["category"];
    $sqlkt = "SELECT * FROM category WHERE name_category='$category'";
    $kt = $conn->query($sqlkt)->rowCount();
    if ($kt == 0) {
        $sql = "INSERT INTO category values(null,'$category',current_timestamp)";
        $kq = $conn->query($sql);
        if ($kq) {
            $_SESSION["thongbao"] = '<div class="alert alert-success" role="alert">
              Thêm mới danh mục thành công !
           </div>';
          header("Location:show_category.php");
          exit();
        } else {
            echo "Thất bại";
        }
    } else {
        $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
         Danh mục đã tồn tại !
        </div>';
    }
}
?>
    <div class="container-fluid">
        <h3 style="padding-top:20px;">Thêm mới danh mục</h3>
        <div><?php if (isset($_SESSION["thongbao"])) {
                    echo $_SESSION["thongbao"];
                    unset($_SESSION["thongbao"]);
                }
                ?></div>
        <form action="" method="post">
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Tên danh mục</label>
                <div class="col-sm-6">
                    <input type="" name="category" class="form-control" id="inputPassword" placeholder="danh mục" required>
                </div>
            </div>
            <input class="btn btn-success" name="luu" style="margin-left:636px" type="submit" value="Lưu">
        </form>
    </div>
</div>

<div class="clear"></div>
<?php include './include/include_footer.php' ?>
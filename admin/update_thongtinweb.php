<?php include './include/include_heder.php' ?>
<div class="container-fluid" style="min-height: 500px">
    <h3 style="padding-top:20px;padding-left:100px;">Cập nhật Slide</h3>
    <div class="container">
    <?php
include './connect.php';
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $sqlupdate = "SELECT*FROM information where id='$id'";
    $kqupdate = $conn->query($sqlupdate)->fetch();
}
if (isset($_POST["luu"])) {
    $logo = $_FILES["images"]["name"];
    $tmpA = $_FILES["images"]["tmp_name"];
    $checkimg = array("","image/png", "image/jpeg", "image/gif");
    $typeimg = $_FILES["images"]["type"];
    $error=$_FILES["images"]["error"];
    if (empty($logo)) {
        $logo =  $kqupdate["logo"];
    }
    $address = $_POST["address"];
    $email = $_POST["email"];
    $phone = $_POST["phone"];
    $license = $_POST["license"];
    if (!in_array("$typeimg", $checkimg) || $error==1 ) {
        $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
            Sai định dạng ảnh!
             </div>';
    } else if ($_FILES["images"]["size"] > 2000000) {
        $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
        Dung lượng ảnh không được vượt quá 2MB!
         </div>';
    } else {
        $sql_information = "UPDATE information
        SET logo='$logo',address='$address',email='$email',phone='$phone', license='$license';
        WHERE id=$id";
        $kq_information = $conn->query($sql_information);
        if ($kq_information) {
            move_uploaded_file($tmpA, "./images/" . $logo);
            $_SESSION["thongbao"] = '<div class="alert alert-success" role="alert">
          Cập nhật thông tin web thành công !
       </div>';
            header("Location:show_thongtinweb.php");
            exit();
        } else {
            echo "Thất bại";
        }
    }
}
?>
    <div class="container-fluid">
        <h3 style="padding-top:20px;">Thêm thông tin</h3>
        <div><?php if (isset($_SESSION["thongbao"])) {
                    echo $_SESSION["thongbao"];
                    unset($_SESSION["thongbao"]);
                }
                ?></div>
        <form action="" method="post"  enctype="multipart/form-data">
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Logo</label>
                <div class="col-sm-6">
                    <input type="file" value="<?php echo $kqupdate["logo"] ?>"  name="images" class="form-control" id="inputPassword" placeholder="Logo" >
                </div>
                <img src="./images/<?php echo $kqupdate["logo"] ?>" alt="" width="200px">
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Địa chỉ</label>
                <div class="col-sm-6">
                    <input type="" value="<?php echo $kqupdate["address"] ?>"  name="address" class="form-control" id="inputPassword" placeholder="Địa chỉ" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-6">
                    <input type="" value="<?php echo $kqupdate["email"] ?>"  name="email" class="form-control" id="inputPassword" placeholder="Email" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Số điện thoại</label>
                <div class="col-sm-6">
                    <input type="" value="<?php echo $kqupdate["phone"] ?>" name="phone" class="form-control" id="inputPassword" placeholder="Số điện thoại" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Chứng nhận</label>
                <div class="col-sm-6">
                    <input type="" value="<?php echo $kqupdate["license"] ?>" name="license" class="form-control" id="inputPassword" placeholder="Chứng nhận" required>
                </div>
            </div>
            <input class="btn btn-success" name="luu" style="margin-left:636px" type="submit" value="Lưu">
        </form>
    </div>
</div>
<div class="clear"></div>
<?php include './include/include_footer.php' ?>
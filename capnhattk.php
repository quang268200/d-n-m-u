<?php session_start() ?>
<?php ob_start() ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cập nhật tài khoản</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <script src="./js/jquery.js"></script>
    <script src='./js/fontawesome.js'></script>
    <script src="./js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="./css/index.css">
</head>

<body>
    <?php include './include/header.php' ?>
    <?php
    if (isset($_POST["capnhat"])) {
        $user = $_POST["user"];
        $images = $_FILES["images"];
        $name_images = $_FILES["images"]["name"];
        $tmpA = $_FILES["images"]["tmp_name"];
        $checkimg = array('', "image/png", "image/jpeg", "image/gif");
        $typeimg = $_FILES["images"]["type"];
        $name = $_POST["name"];
        $address = $_POST["address"];
        $error=$_FILES["images"]["error"];
        if (empty($name_images)) {
            $name_images = $info_user["avatar"];
        }
        if (!in_array("$typeimg", $checkimg) || $error==1) {
            $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
        Sai định dạng ảnh!
       </div>';
        } else if ($_FILES["images"]["size"] > 2000000) {
            $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
            Dung lượng ảnh không được vượt quá 2MB!
           </div>';
        } else if (mb_strlen($_POST["name"]) <= 5 || mb_strlen($_POST["name"]) >= 20) {
            $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
            Tên phải lớn hơn 5 nhỏ hơn 20 kí tự!
           </div>';
        } else {
            $sql_update_thongtin = "UPDATE user set avatar='$name_images',name='$name',address='$address' where user='$user'";
            $kq_update_thongtin = $conn->query($sql_update_thongtin);
            if ($kq_update_thongtin) {
                move_uploaded_file($tmpA, "./admin/images/" . $name_images);
                $_SESSION["thongbao"] = "<script>
                function dangki() {
                    Swal.fire(
                        'Cập nhật thông tin thành công!',
                        'You clicked the button!',
                        'success'
                    )
                }
                dangki()
            </script>";
                header("Refresh:0");
                exit();
            }
        }
    }
    ?>
    <div class="full">
        <div class="container-fluid can_thongtin_tk ">
            <div class="title_mk">
                <h4>Hồ sơ của tôi</h4>
                <p>Quản lý thông tin hồ sơ thông tin cá nhân của bạn</p>
            </div>
            <hr>
            <form method="post" enctype="multipart/form-data">
                <?php
                if (isset($_SESSION["thongbao"])) {
                    echo  $_SESSION["thongbao"];
                    unset($_SESSION["thongbao"]);
                }
                ?>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">Tài khoản</label>
                    <div class="col-sm-9">
                        <input type="" class="form-control" id="inputPassword" name="user" readonly="readonly" placeholder="" required value="<?php echo $info_user["user"] ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">Ảnh đại diện </label>
                    <div class="col-sm-7">
                        <input type="file" class="form-control" id="inputPassword" name="images" placeholder="">
                    </div>
                    <img src="./admin/images/<?php echo $info_user["avatar"] ?>" alt="" width="100px">
                </div>

                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">Họ và tên</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="inputPassword" name="name" placeholder="" required value="<?php echo $info_user["name"] ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">Địa chỉ</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="inputPassword" name="address" placeholder="" required value="<?php echo $info_user["address"] ?>">
                    </div>
                </div>
                <button type="submit" class="btn btn-danger" name="capnhat" style="float: right">Cập nhật</button>
            </form>
            <div class="clear"></div>
        </div>

    </div>
    <?php include './include/footer.php' ?>
    <a href="#click"><input id='btn' /></a>
    <script>
        setTimeout(function() {
            document.getElementById('btn').click();
        }, 0);
    </script>
</body>

</html>
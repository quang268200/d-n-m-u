<?php session_start() ?>
<?php ob_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Trang chủ</title>
  <link rel="stylesheet" href="./css/bootstrap.min.css">
  <script src="./js/jquery.js"></script>
  <script src='./js/fontawesome.js'></script>
  <script src="./js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="./css/index.css">
</head>

<body>
  <?php include './include/header.php' ?>
  <div class="main ">
    <div class="row">
      <?php include './include/content.php' ?>
      <?php include './include/slidebar.php' ?>
    </div>
  </div>
  <?php include './include/footer.php' ?>

</body>

</html>
<?php include './include/include_heder.php' ?>
<?php include './include/include_sidebar.php' ?>
<div class="main-right">
    <?php
    include './connect.php';
    if (isset($_POST["luu"])) {
        $name_slide = $_POST["name_slide"];
        $name = $_FILES["images"]["name"];
        $tmpA = $_FILES["images"]["tmp_name"];
        $checkimg = array("image/png", "image/jpeg", "image/gif");
        $typeimg = $_FILES["images"]["type"];
        $link = $_POST["link"];
        $special = 0;
        if (!in_array("$typeimg", $checkimg)) {
            $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
                Sai định dạng ảnh!
                 </div>';
        } else if ($_FILES["images"]["size"] > 2000000) {
            $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
            Dung lượng ảnh không được vượt quá 2MB!
             </div>';
        } else {
            $sql = "INSERT INTO slide values(null,'$name','$name_slide','$special','$link',current_timestamp)";
            $kq = $conn->query($sql);
            if ($kq) {
                move_uploaded_file($tmpA, "./images/" . $name);
                $_SESSION["thongbao"] = '<div class="alert alert-success" role="alert">
              Thêm mới slide thành công !
           </div>';
           header("Location:show_slide.php");
            } else {
                echo "Thất bại";
            }
        }
    }
    ?>
    <div class="container-fluid">
        <h3 style="padding-top:20px;">Thêm mới Slide</h3>
        <div><?php if (isset($_SESSION["thongbao"])) {
                    echo $_SESSION["thongbao"];
                    unset($_SESSION["thongbao"]);
                }
                ?></div>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Tên silde</label>
                <div class="col-sm-6">
                    <input type="" name="name_slide" class="form-control" id="inputPassword" placeholder="tên silde" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Imgages</label>
                <div class="col-sm-6">
                    <input type="file" name="images" class="form-control" id="inputPassword" placeholder="images" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Link</label>
                <div class="col-sm-6">
                    <input type="text" name="link" class="form-control" id="inputPassword" placeholder="link" required>
                </div>
            </div>
            <input class="btn btn-success" name="luu" style="margin-left:636px" type="submit" value="Lưu">
        </form>
    </div>
</div>

<div class="clear"></div>
<?php include './include/include_footer.php' ?>
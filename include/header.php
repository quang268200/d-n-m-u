<link rel="stylesheet" href="./css/sweetalert.css">
<script src="./js/sweetalert.js"></script>
<div class="center">
  <div class="header">
    <div class="taikhoan info">
      <?php
      include './admin/connect.php';
      $sql_thongtinweb="SELECT * FROM information limit 0,1";
      $kq_thongtinweb=$conn->query($sql_thongtinweb)->fetch();
      if (isset($_SESSION["user"])) {
        $tk = $_SESSION["user"];
        $sql_user = "SELECT * FROM user where user='$tk'";
        $info_user = $conn->query($sql_user)->fetch();
        echo '<span id="thongtin"><a href="./dangki/index.php" data-toggle="modal" data-target="#exampleModal">Tài khoản</a>
         <ul class="doimk">
         <a href="doimk.php"><li>Đổi mật khẩu</li></a>
         <a href="capnhattk.php"><li>Cập nhật tài khoản</li></a>
        </ul>
         </span>';
        echo  '<span id="name_user"> <img src="./admin/images/' . $info_user["avatar"] . '" alt=""><a href="" >' . $info_user["name"] . '</a></span>';
        echo '<span id=""><a href="./dangxuat.php">Đăng xuất</a></span>';
        if ($info_user["permission"] != 0) {
          echo '<span id=""><a href="./admin/index.php">Quản trị</a></span>';
        }
      } else {
        echo '<span id="dangki"><a href="./dangki/index.php">Đăng kí</a></span>
        <span id="dangnhap"><a href="" data-toggle="modal" data-target="#myModal">Đăng nhập</a></span>';
      }
      ?>
    </div>
    <div class="logo-search">
      <div class="logo"><a href="index.php"><img src="./admin/images/<?php echo $kq_thongtinweb["logo"] ?>" alt="" width="250px" height="65px"></a></div>
      <div class="search">
        <form action="search.php?keyword=<?php $_GET["keyword"] ?>" method="GET">
          <input type="text" placeholder="Tìm theo tên sản phẩm" name="keyword" required>
          <button><i style="color: #ffff;font-size: 18px" class="fab fa-sistrix"></i></button>
        </form>
      </div>
      <i style="font-size: 25px; color: #fff" class="fa fa-shopping-cart"></i>

    </div>
  </div>
  <div class="banner">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <?php
        $dem_slide = "SELECT count(id_slide) FROM slide  where special=1";
        $dem_slide = $conn->query($dem_slide)->fetchColumn();
        // echo $dem_slide;
        // exit();
        for ($i = 0; $i < $dem_slide; $i++) {
          ?>
          <?php
            if ($i == $dem_slide) {
              echo '<li data-target="#carouselExampleIndicators" data-slide-to="' . $i . '" class="active"></li>';
            } else {
              echo '<li data-target="#carouselExampleIndicators" data-slide-to="' . $i . '" class=""></li>';
            }
            ?>
        <?php
        }
        ?>
        <!-- <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="4" class=""></li> -->
      </ol>
      <div class="carousel-inner">
        <?php
        $sql_slide = "SELECT * FROM slide  where special=1";
        $sql_slide = $conn->query($sql_slide);
        $dem = 0;
        foreach ($sql_slide as $key => $value) {
          $dem++;
          ?>
          <a href="#"></a>
          <?php
            if ($dem == 2) {

              echo '<div class="carousel-item active">
              <a href="' . $value["link"] . '"><img class="d-block w-100" src="./admin/images/' . $value["images"] . '"height="400px;"></a>
            </div>';
            } else {
              echo '<div class="carousel-item">
              <a href="' . $value["link"] . '"> <img class="d-block w-100" src="./admin/images/' . $value["images"] . '"height="400px;"></a>
            </div>';
            }
            ?>

        <?php
        }
        ?>
        <!-- <div class="carousel-item">
          <img class="d-block w-100" src="./images/banner2.jpg" height="400px;">
        </div>
        <div class="carousel-item active">
          <img class="d-block w-100" src="./images/banner3.jpeg" height="400px;">
        </div> -->
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
  <div class="menu" id="click">
    <ul>
      <li><a href="index.php">TRANG CHỦ</a></li>
      <li><a href="#">GIỚI THIỆU</a></li>
      <li><a href="#">SẢN PHẨM</a></li>
      <li><a href="#">LIÊN HỆ</a></li>
      <li><a href="#">GÓP Ý</a></li>
    </ul>
  </div>
  <?php include './include/dangnhap.php' ?>
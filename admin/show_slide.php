<?php include './include/include_heder.php' ?>
<?php include './include/include_sidebar.php' ?>
<div class="container">
    <div class="main-right">
        <h2 style="padding-top:20px;">Slide &nbsp <a href="add_slide.php" class="btn btn-success">Thêm slide</a></h2>
        <?php
        if (isset($_SESSION["thongbao"])) {
            echo $_SESSION["thongbao"];
            unset($_SESSION["thongbao"]);
        }
        ?>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>ID</th>
                    <th>Ảnh</th>
                    <th>Tên slide</th>
                    <th>Đặc biệt</th>
                    <th>Link</th>
                    <th>Ngày tạo</th>
                    <th>Sửa</th>
                    <th>Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                include './connect.php';
                // start đếm tổng số lượng user
                $sql_count = "SELECT count(id_slide) from slide";
                $kq_count = $conn->query($sql_count)->fetchColumn();
                //end đếm tổng số lượng user
                if (!isset($_GET["page"])) {
                    $page = 1;
                } else {
                    $page = $_GET["page"];
                }
                $limit = 5;
                $start = ($page - 1) * $limit;
                $total_page = ceil($kq_count / $limit);
                $sql = "SELECT * from slide limit $start,$limit ";
                $kq = $conn->query($sql);
                $stt = 0;
                foreach ($kq as $key => $value) {
                    $stt = $start += 1;
                    ?>
                    <tr>
                        <td><?php echo $stt ?></td>
                        <td><?php echo $value["id_slide"] ?></td>
                        <td><img width="200px" src="./images/<?php echo $value["images"] ?>" alt=""></td>
                        <td><?php echo $value["name_slide"] ?></td>
                        <td><?php if ($value["special"] == 1) {
                                    echo '  <a href="show_off_slide.php?id=' . $value["id_slide"] . '&&page=' . $page . '" class="btn btn-success">Hiện</a>';
                                } else {
                                    echo '  <a href="show_off_slide.php?id=' . $value["id_slide"] . '&&page=' . $page . '" class="btn btn-primary">Ẩn</a>';
                                }
                                ?></td>
                        <td><?php echo $value["link"] ?></td>
                        <td><?php echo $value["create_at"] ?></td>
                        <td style="text-align: center"><a href="./update_slide.php?id=<?php echo $value['id_slide'] ?>&&page=<?php echo $page ?>"><i class="fa fa-clipboard"></i> </a></td>
                        <td style="text-align: center" onclick="return confirm('Bạn có chắc chắn muốn xóa')"><a href="./delete/delete_slide.php?id=<?php echo $value['id_slide'] ?>&&page=<?php echo $page ?>"><i class="fa fa-trash-alt"></i> </a></td>

                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        
        <ul class="pagination">
            <div class="phantrang">
                <?php
                if ($page > 1) {
                    ?>
                    <li class="page-item"><a class="page-link" href="show_slide.php?page=<?php echo ($page - 1) ?>">Previous</a></li>
                <?php
                }
                ?>

                <?php
                for ($i = 1; $i <= $total_page; $i++) {
                    if ($i == $page) {
                        echo  '<li class="page-item active"><a class="page-link" >' . $i . '</a></li>';
                    } else {
                        echo  '<li class="page-item"><a class="page-link" href="show_slide.php?page=' . $i . '">' . $i . '</a></li>';
                    }
                }
                ?>
                <?php
                if ($page < $total_page) {
                    ?>
                    <li class="page-item"><a class="page-link" href="show_slide.php?page=<?php echo ($page + 1) ?>">Next</a></li>
                <?php
                }
                ?>
            </div>
        </ul>
    </div>

</div>

<div class="clear"></div>

<?php include './include/include_footer.php' ?>
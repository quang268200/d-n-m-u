<div class="main-left col-md-9">
  <div class="sanpham">
    <p>Danh sách sản phẩm</p>
  </div>
  <ul>
    <?php include './admin/connect.php';
    $limit = 12;
    //start đếm tổng số lượng sản phẩm được show ra
    $sql_count_products = "SELECT count(id_product) FROM products where special=1";
    $kq_conut_products = $conn->query($sql_count_products)->fetchColumn();
    //end đếm tổng số lượng sản phẩm được show ra
    $total_page=ceil($kq_conut_products/$limit);
    if (isset($_GET["page"])) {
      $current_page = $_GET["page"];
    } else {
      $current_page = 1;
    }
    $start = ($current_page - 1) * $limit;
    $sql_show_products = "SELECT * FROM products where special=1  limit $start,$limit";
    $kq_show_products = $conn->query($sql_show_products);
    foreach ($kq_show_products as $key => $value) {
      ?>
      <li>
        <a href="chitietsp.php?id=<?php echo $value["id_product"] ?>">
          <div class="anhsp">
            <img src="./admin/images/<?php echo $value["images"] ?>" alt="" width="170px" height="170px">
          </div>
          <h5><?php echo $value['name_product'] ?></h5>
          <p><?php echo number_format($value['sale']) ?> đ</p>
          <span><del><?php echo number_format($value['price']) ?> đ</del></span>
        </a>
      </li>
    <?php
    }
    ?>
  </ul>
  <div class="clear"></div>
  <div class="phantrang">
    <ul class="pagination ">
      <?php
      if ($total_page > 1) {
        if ($current_page > 1) {
          echo '<li class="page-item"><a class="page-link" href="index.php?page=' . ($current_page - 1) . '">Previous</a></li>';
        }
        for ($i = 1; $i <= $total_page; $i++) {
          if ($current_page == $i) {
            echo '<li class="page-item active"><a class="page-link " href="">' . $i . '</a></li>';
          } else {
            echo '<li class="page-item"><a class="page-link" href="index.php?page=' . $i . '">' . $i . '</a></li>';
          }
        }
        if ($current_page < $total_page) {
          echo '<li class="page-item"><a class="page-link" href="index.php?page=' . ($current_page + 1) . '">next</a></li>';
        }
      }
      ?>

    </ul>
  </div>
</div>
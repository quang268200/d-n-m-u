<div class="main">
            <nav id="sidebar">

                <ul class="list-unstyled components">
                    <p>Dummy Heading</p>
                    <li class="active">
                        <a href="#categorySubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Danh mục</a>
                        <ul class="collapse list-unstyled" id="categorySubmenu">
                            <li>
                                <a href="add_category.php">Thêm danh mục</a>
                            </li>
                            <li>
                                <a href="show_category.php">Show danh mục</a>
                            </li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Sản phẩm</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li>
                                <a href="add_products.php">Thêm sản phẩm</a>
                            </li>
                            <li>
                                <a href="show_products.php">Show sản phẩm</a>
                            </li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="#aboutSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">User</a>
                        <ul class="collapse list-unstyled" id="aboutSubmenu">
                     
                            <li>
                                <a href="show_user.php">Show user</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Slides</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a href="add_slide.php">Thêm slie</a>
                            </li>
                            <li>
                                <a href="show_slide.php">Cập nhật</a>
                            </li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="#video" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Thông tin web</a>
                        <ul class="collapse list-unstyled" id="video">
                            <li>
                                <a href="add_thongtinweb.php">Thông tin</a>
                            </li>
                            <li>
                                <a href="show_thongtinweb.php">Chỉnh sửa</a>
                            </li>
                           
                        </ul>
                    </li>
                    <li class="active">
                        <a href="#quangcao" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Quản lý comment</a>
                        <ul class="collapse list-unstyled" id="quangcao">
                            <li>
                                <a href="show_comment.php">Comment</a>
                            </li> 
                        </ul>
                    </li>
                </ul>

            </nav>
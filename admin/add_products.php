<?php include './include/include_heder.php' ?>
<?php include './include/include_sidebar.php' ?>
<div class="main-right">
    <div class="container-fluid">
        <?php
        include './connect.php';
        if (isset($_POST["luu"])) {
            $name_product = $_POST["name_product"];
            $name = $_FILES["images"]["name"];
            $tmpA = $_FILES["images"]["tmp_name"];
            $checkimg = array("image/png", "image/jpeg", "image/gif");
            $typeimg = $_FILES["images"]["type"];
            $price = $_POST["price"];
            $sale = $_POST["sale"];
            $amount = $_POST["amount"];
            if ($price < 0) {
                $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
               Giá sản phẩm phải lớn hơn 0!
              </div>';
            } else if ($sale < 0) {
                $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
                Giá khuyến mãi phải lớn hơn 0!
               </div>';
            } else if ($amount < 0) {
                $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
              Số lượng phải lớn hơn 0!
               </div>';
            } else if (!in_array("$typeimg", $checkimg)) {
                $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
                Sai định dạng ảnh!
                 </div>';
            } else if ($_FILES["images"]["size"] > 2000000) {
                $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
                Dung lượng ảnh không được vượt quá 2MB!
                 </div>';
            } else {
                move_uploaded_file($tmpA, "./images/" . $name);
                $detail = $_POST["detail"];
                $id_category = $_POST["id_category"];
                $sql_prpduct = "INSERT INTO products(id_product,name_product,images,price,sale,amount,detail,id_category,create_at) value(null,'$name_product','$name',$price,'$sale','$amount','$detail','$id_category',current_timestamp)";
                $kq_product = $conn->query($sql_prpduct);
                if ($kq_product) {
                    $_SESSION["thongbao"] = '<div class="alert alert-success" role="alert">
       Thêm mới sản phẩm thành công !
     </div>';
                    header("Location:show_products.php");
                    exit();
                } else {
                    $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
  Thêm mới sản phẩm thất bại!
 </div>';
                }
            }
        }
        ?>
        <h3 style="padding-top:20px;">Thêm mới sản phẩm</h3>
        <div><?php if (isset($_SESSION["thongbao"])) {
                    echo $_SESSION["thongbao"];
                    unset($_SESSION["thongbao"]);
                }
                ?></div>
        <form action="" method="post" enctype="multipart/form-data">

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Danh mục</label>
                <div class="col-sm-6">
                    <select id="inputState" class="form-control col-sm-12" required name="id_category">
                        <!-- <option selected>Choose...</option> -->
                        <?php

                        $sql = "SELECT * FROM category";
                        $kq = $conn->query($sql);
                        foreach ($kq as $key => $value) {
                            ?>
                            <option value="<?php echo $value["id_category"] ?>"><?php echo $value["name_category"] ?></option>
                        <?php
                        }
                        ?>

                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Tên sản phẩm</label>
                <div class="col-sm-6">
                    <input type="" name="name_product" class="form-control" id="inputPassword" placeholder="tên sản phẩm" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Số lượng</label>
                <div class="col-sm-6">
                    <input type="number" name="amount" class="form-control" id="inputPassword" placeholder="số lượng" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Giá sản phẩm</label>
                <div class="col-sm-6">
                    <input type="number" name="price" class="form-control" id="inputPassword" placeholder="giá sân phẩm" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Giảm giá</label>
                <div class="col-sm-3">
                    <input type="number" name="sale" class="form-control" id="inputPassword" placeholder="giảm giá">
                </div>
                <label for="inputPassword" class="col-sm-1 col-form-label">Images</label>
                <div class="col-sm-3">
                    <input type="file" name="images" class="form-control" id="inputPassword" required>
                </div>
            </div>

            <div class="form-group">
                <label for="exampleFormControlTextarea1">Nội dung chi tiết</label>
                <textarea name="detail" class="form-control" id="detail" rows="3"></textarea>
            </div>
            <script>
                CKEDITOR.replace('detail');
            </script>
            <input class="btn btn-success" name="luu" style="margin-left:" type="submit" value="Lưu">
        </form>

    </div>
</div>

<div class="clear"></div>
<?php include './include/include_footer.php' ?>
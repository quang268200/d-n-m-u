<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thông tin tài khoản</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-5 " style="margin-bottom: 1rem !important;">
                            <label data-error="wrong" data-success="right" for="orangeForm-name">Tài khoản</label>
                            <input type="text" id="orangeForm-name" class="form-control validate" name="tk" required>
                        </div>
                        <div class="md-form mb-4">
                            <label data-error="wrong" data-success="right" for="orangeForm-pass">Mật khẩu</label>
                            <input type="password" id="orangeForm-pass" class="form-control validate" name="mk" required>
                        </div>
                        <span style="cursor: pointer">Quên mật khẩu</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>
        </div>
    </div>
</div>
<?php session_start() ?>
<?php ob_start()?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!-- <script src='https://kit.fontawesome.com/a076d05399.js'></script> -->
    <script src="./js/fontawesome.js"></script>
  <script src="./ckeditor/ckeditor.js"></script>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/index.css">
</head>

<body>
    <?php include './include/permission.php'?>
    <div class="center">
        <div class="header">
            <h3>Quản trị hệ thống</h3>
           <span style="color:white; margin-left: 66px"><a href="../index.php">Trang chủ</a></span>
            <?php if(isset($_SESSION["user"])){
                ?>
                <p style="color:white" class="dangnhap">Xin chào <?php echo $_SESSION["user"] ?><span><a href="../dangxuat.php">Đăng xuất</a></span></p>

            
            <?php
           } ?> 
        </div>
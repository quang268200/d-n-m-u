<?php include './include/include_heder.php' ?>
<?php include './include/include_sidebar.php' ?>
<div class="main-right">
    <div class="container-fluid">
        <h3 style="padding-top:20px;">Show thông tin website  <a href="add_thongtinweb.php" class="btn btn-success">Thêm thông tin</a></h3>
        <div class="container">
        <div class="test"><?php if (isset($_SESSION["thongbao"])) {
                    echo $_SESSION["thongbao"];
                    unset($_SESSION["thongbao"]);
                }
                ?></div>
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Logo</th>
                        <th>Địa chỉ</th>
                        <th>Email</th>
                        <th>Số điện thoại</th>
                        <th>Chứng nhận</th>
                        <th style="text-align: center" >Sửa </th>
                        <th style="text-align: center" >Xóa</th>
                        
                    </tr>
                </thead>
                <tbody>
                   <?php
                   include './connect.php';
                   $sql_show_thongtinweb="SELECT * FROM information";
                   $kq_show_thongtinweb=$conn->query($sql_show_thongtinweb);
                   foreach ($kq_show_thongtinweb as $key => $value) {
                       ?>
                             <tr>
                            <td><?php echo $value["id"] ?></td>
                            <td><img width="200px" src="./images/<?php echo $value["logo"] ?>" ></td>
                            <td><?php echo $value["address"] ?></td>
                            <td><?php echo $value["email"] ?></td>
                            <td><?php echo $value["phone"] ?></td>
                            <td><?php echo $value["license"] ?></td>
                            <td style="text-align: center" ><a href="./update_thongtinweb.php?id=<?php echo $value["id"]?>"><i class="fa fa-clipboard"></i></a></td>
                            <td style="text-align: center" onclick="return confirm('Bạn có chắc chắn muốn xóa')" ><a href="./delete/delete_thongtinweb.php?id=<?php echo $value["id"]?>"><i class="fa fa-trash-alt"></i> </a></td>

                        </tr>
                   <?php }
                   ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="clear"></div>
<?php include './include/include_footer.php' ?>
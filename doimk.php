<?php session_start() ?>
<?php ob_start() ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đổi mật khẩu</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <script src="./js/jquery.js"></script>
    <script src='./js/fontawesome.js'></script>
    <script src="./js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="./css/index.css">
</head>


<body>
    <?php include './include/header.php' ?>
    <?php
    include './admin/connect.php';
    if (isset($_POST["xacnhan"])) {
        $user = $_POST["user"];
        $mk = md5($_POST["pass"]);
        $new_mk = md5($_POST["new_pass"]);
        $repeat_mk = md5($_POST["repeat_pass"]);
        // echo $mk;
        $sql_kt_mk = "SELECT * FROM user where user='$user' and password='$mk'";
        $kq_kt_mk = $conn->query($sql_kt_mk)->rowCount();
        if ($kq_kt_mk < 1) {
            $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
        Mật khẩu bạn nhập không đúng!
       </div>';
        } else if (mb_strlen($_POST["new_pass"]) <= 5 || mb_strlen($_POST["new_pass"]) >= 10) {
            $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
        Mật khẩu phải có độ dài từ 5 đến 10 kí tự !
       </div>';
        } else if ($new_mk != $repeat_mk) {
            $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
        Mật khẩu bạn nhập lại không giống nhau!
       </div>';
        } else {
            $sql_update_pass = "UPDATE user set password='$new_mk' where user='$user'";
            $kq_update_pass = $conn->query($sql_update_pass);
            if ($kq_update_pass) {
                $_SESSION["thongbao"] = "<script>
            function dangki() {
                Swal.fire(
                    'Đổi mật khẩu thành công!',
                    'You clicked the button!',
                    'success'
                )
            }
            dangki()
        </script>";
                header("Refresh:0");
                exit();
            }
        }
    }
    ?>
    <div class="full">
        <div class="container-fluid can_thongtin_tk ">
            <div class="title_mk">
                <h4>Đổi mật khẩu</h4>
                <p>Để bảo mật tài khoản, vui lòng không chia sẻ mật khẩu cho người khác</p>
            </div>
            <hr>
            <form method="post">
                <?php
                if (isset($_SESSION["thongbao"])) {
                    echo  $_SESSION["thongbao"];
                    unset($_SESSION["thongbao"]);
                }
                ?>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">Tài khoản</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="inputPassword" readonly="readonly" name="user" placeholder="" required value="<?php echo $info_user["user"] ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">Mật khẩu </label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="pass" id="inputPassword" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">Mật khẩu mới</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="new_pass" id="inputPassword" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">Xác nhận mật khẩu</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="repeat_pass" id="inputPassword" placeholder="" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-danger" name="xacnhan" style="float: right">Xác nhận</button>
            </form>
            <div class="clear"></div>
        </div>
    </div>
    <?php include './include/footer.php' ?>
    <a href="#click"><input id='btn'/></a>
    <script>
        setTimeout(function() {
            document.getElementById('btn').click();
        }, 0);
    </script>
</body>

</html>
<?php include './include/include_heder.php' ?>
<div class="container-fluid" style="min-height: 500px">
    <h3 style="padding-top:20px;padding-left:100px;">Cập nhật Slide</h3>
    <div class="container">
        <?php
        include './connect.php';
        if (isset($_GET["page"])) {
            $page = $_GET["page"];
            $link = './show_slide.php?page=' . $page;
        } else {
            $link = './show_slide.php';
        }
        if (isset($_GET["id"])) {
            $id = $_GET["id"];
            $sqlupdate = "SELECT*FROM slide where id_slide='$id'";
            $ktupdate = $conn->query($sqlupdate)->fetch();
        }
        ?>
        <div><?php if (isset($_SESSION["thongbao"])) {
                    echo $_SESSION["thongbao"];
                }
                ?></div>
        <?php
        include './connect.php';
        if (isset($_POST["luu"])) {
            $name_slide = $_POST["name_slide"];
            $name = $_FILES["images"]["name"];
            $tmpA = $_FILES["images"]["tmp_name"];
            $checkimg = array("", "image/png", "image/jpeg", "image/gif");
            $typeimg = $_FILES["images"]["type"];
            $linksp = $_POST["link"];
            $special = 0;
            $error=$_FILES["images"]["error"];
            if (empty($name)) {
                $name = $ktupdate["images"];
            }
            if (!in_array("$typeimg", $checkimg) || $error==1) {
                $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
                Sai định dạng ảnh!
                 </div>';
            } else if ($_FILES["images"]["size"] > 2000000) {
                $_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
            Dung lượng ảnh không được vượt quá 2MB!
             </div>';
            } else {
                $sql = "UPDATE slide
                SET name_slide='$name_slide',images='$name',link='$linksp'
                WHERE id_slide=$id ";
                $kq = $conn->query($sql);
                if ($kq) {
                    move_uploaded_file($tmpA, "./images/" . $name);
                    $_SESSION["thongbao"] = '<div class="alert alert-success" role="alert">
             Cập nhật slide thành công !    
           </div>';
                    header('Location:' . $link);
                    exit();
                } else {
                    echo "Thất bại";
                }
            }
        }
        ?>
        <div><?php if (isset($_SESSION["thongbao"])) {
                    echo $_SESSION["thongbao"];
                    unset($_SESSION["thongbao"]);
                }
                ?></div>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Tên silde</label>
                <div class="col-sm-6">
                    <input type="" name="name_slide" class="form-control" id="inputPassword" value="<?php echo $ktupdate["name_slide"] ?>" placeholder="tên silde" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Imgages</label>
                <div class="col-sm-6">
                    <input type="file" name="images" class="form-control" id="inputPassword" placeholder="images">
                </div>
                <img src="./images/<?php echo $ktupdate["images"] ?>" alt="" width="200px">
            </div>

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Link</label>
                <div class="col-sm-6">
                    <input type="text" name="link" class="form-control" id="inputPassword" placeholder="link" value="<?php echo $ktupdate["link"] ?>" required>
                </div>
            </div>
            <input class="btn btn-success" name="luu" style="margin-left:636px" type="submit" value="Lưu">
        </form>
    </div>
</div>
</div>

</div>

</div>
<div class="clear"></div>
<?php include './include/include_footer.php' ?>
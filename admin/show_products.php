<?php include './include/include_heder.php' ?>
<style>
th,td{
    text-align: center;
}
</style>
<?php include './include/include_sidebar.php' ?>
<div class="main-right">
    <div class="container-fluid">
        <h3 style="padding-top:20px;">Show sản phẩm  <a href="add_products.php" class="btn btn-success">Thêm sản phẩm</a></h3>
        <div class="container">
            <div class="test"><?php if (isset($_SESSION["thongbao"])) {
                                    echo $_SESSION["thongbao"];
                                    unset($_SESSION["thongbao"]);
                                }
                                ?></div>
            <table class="table">
                <thead>
                    <tr>
                        <th>ID sản phẩm</th>
                        <th>Tên sản phẩm</th>
                        <th>Ảnh sản phẩm</th>
                        <th>Info</th>
                        <th>Giảm giá </th>
                        <th>Hiển thị</th>
                        <th>Danh mục</th>
                        <th style="text-align: center">Sửa </th>
                        <th style="text-align: center">Xóa</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    include './connect.php';
                    $limit = 4;
                    if (isset($_GET["page"])) {
                        $current_page = $_GET["page"];
                    } else {
                        $current_page = 1;
                    }
                    $start = ($current_page - 1) * $limit;
                    // đếm số trang;
                    $sql_count = "SELECT COUNT(id_product) from products";
                    $kq_count = $conn->query($sql_count)->fetchColumn();
                    $total_page = ceil($kq_count / $limit);
                    $sql = "SELECT * FROM  products   limit $start,$limit";
                  //  ORDER BY create_at DESC
                    $kq = $conn->query($sql);
                    foreach ($kq as $key => $value) {
                        ?>
                        <tr>
                            <td><?php echo $value["id_product"] ?></td>
                            <td style="width: 200px"><?php echo $value["name_product"] ?></td>
                            <td><img width="100px;" height="100px" src="./images/<?php echo $value["images"] ?>" alt=""></td>
                            <td>
                                <ul>
                                    <li>
                                        <p> Giá : <?php echo number_format($value["price"]) ?> đ</p>
                                    </li>
                                    <li>
                                        <p> Số lượng: <?php echo $value["amount"] ?> </p>
                                    </li>
                                    <li>
                                        <p> Lượt xem: <?php echo $value["view"] ?> </p>
                                    </li>
                                </ul>
                            </td>
                            <td> <?php echo number_format($value["sale"]) ?></td>
                            <!-- start hiện thị hay không -->
                            <td>
                                <a class="btn <?php
                                                    if ($value["special"] == 0) {
                                                        echo "btn-danger";
                                                    } else {
                                                        echo "btn-success";
                                                    }
                                                    ?>" href="hienthi.php?id=<?php echo $value["id_product"] ?>&&page=<?php echo $current_page ?>">
                                    <?php
                                        if ($value["special"] == 0) {
                                            echo "Ẩn";
                                        } else {
                                            echo "Hiện";
                                        }
                                        ?>
                                </a>
                            </td>
                            <!-- end hiện thị hay không -->
                            <td>
                                <?php
                                    $id = $value['id_category'];
                                    $sql_category = "SELECT name_category from category where id_category=$id";
                                    $kq_category = $conn->query($sql_category)->fetch();
                                    if ($kq_category) {
                                        echo $kq_category["name_category"];
                                    } else {
                                        echo "Chưa có danh mục";
                                    }
                                    ?>
                            </td>
                            <td style="text-align: center"><a href="./update_products.php?id=<?php echo $value["id_product"] ?>&&page=<?php echo $current_page ?>"><i class="fa fa-clipboard"></i></a></td>
                            <td style="text-align: center" onclick="return confirm('Bạn có chắc chắn muốn xóa')"><a href="./delete/delete_products.php?id=<?php echo $value["id_product"] ?>&&page=<?php echo $current_page ?>"><i class="fa fa-trash-alt"></i> </a></td>
                        </tr>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
            <?php
            $page = 1;
            ?>
            <ul class="pagination">
                <div class="phantrang">
                    <?php
                    if ($current_page > 1) {
                        echo '<li class="page-item"><a class="page-link" href="show_products.php?page=' . ($current_page - 1) . '">Previous</a></li>';
                    }
                    for ($i = 1; $i <= $total_page; $i++) {
                        if ($current_page == $i) {
                            echo   '<li class="page-item active"><a class="page-link">' . $i . '</a></li>';
                        } else {
                            echo   '<li class="page-item"><a class="page-link" href="show_products.php?page=' . $i . '">' . $i . '</a></li>';
                        }
                    }
                    if ($current_page < $total_page) {
                        echo '<li class="page-item"><a class="page-link" href="show_products.php?page=' . ($current_page + 1) . '">Next</a></li>';
                    }
                    ?>
                </div>
            </ul>
        </div>

    </div>
</div>

<div class="clear"></div>
<?php include './include/include_footer.php' ?>
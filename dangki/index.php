<?php ob_start() ?>
<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Đăng kí</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--===============================================================================================-->
	<!--===============================================================================================-->
	<link rel="stylesheet" href="./css/sweetalert.css">
	<script src="./js/sweetalert.js"></script>
	<!--===============================================================================================-->
</head>

<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(images/bg-01.jpg);">
					<span class="login100-form-title-1">
						Đăng kí
					</span>
				</div>
				<?php
				include '../admin/connect.php';
				if (isset($_POST["gui"])) {
					$tk = $_POST["username"];
					$mk = md5($_POST["pass"]);
					$rpmk = md5($_POST["rppass"]);
					//start check img
					$images = $_FILES["images"];
					$name_images = $_FILES["images"]["name"];
					$tmpA = $_FILES["images"]["tmp_name"];
					$checkimg = array('', "image/png", "image/jpeg", "image/gif");
					$typeimg = $_FILES["images"]["type"];
					if (empty($name_images)) {
						$name_images = 'user.png';
					}
					//end check img
					$name = $_POST["name"];
					$address = $_POST["address"];
					$lever = 0;
					if (!in_array("$typeimg", $checkimg)) {
						$_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
						File ảnh không hợp lệ !
					 </div>';
					} else if (mb_strlen($tk) < 5 || mb_strlen($tk) > 20) {
						$_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
						Tài khoản phải từ 5 đến 20 kí tự !
					 </div>';
					} else if (mb_strlen($_POST["pass"]) < 5 || mb_strlen($_POST["pass"]) > 20) {
						$_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
						Mật khẩu phải từ 5 đến 20 kí tự !
					 </div>';
					} else if (mb_strlen($_POST["name"]) < 5 || mb_strlen($_POST["name"]) > 20) {
						$_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
						Tên phải từ 5 đến 20 kí tự !
					 </div>';
					} else if ($mk != $rpmk) {
						$_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
					Mật khẩu xác thực không giống nhau !
					 </div>';
					} else if ($_FILES["images"]["size"] > 2000000) {
						$_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
						Dung lượng ảnh không được vượt quá 2MB !
					 </div>';
					} else {
						$sql = "SELECT *from user where user='$tk'";
						$kq = $conn->query($sql)->rowCount();
						if ($kq == 0) {
							$sqltt = "INSERT INTO user values('$tk','$mk','$name_images','$name','$address','$lever',current_timestamp)";
							$kqtt = $conn->query($sqltt);
							if ($kqtt) {
								move_uploaded_file($tmpA, "../admin/images/" . $name_images);
								$_SESSION["user"] = $tk;
								$_SESSION["thongbao"] = "<script>
									function dangki() {
										Swal.fire(
											'Đăng kí thành công!',
											'You clicked the button!',
											'success'
										)
									}
									dangki()
								</script>";
								header("Location:../index.php");
								exit();
							} else {
								echo "Lỗi";
							}
						} else {
							$_SESSION["thongbao"] = '<div class="alert alert-danger" role="alert">
							Tài khoản đã tồn tại !
						 </div>';
						}
					}
				}
				?>
				  <div><?php if (isset($_SESSION["thongbao"])) {
                    echo $_SESSION["thongbao"];
                    unset($_SESSION["thongbao"]);
                }
                ?></div>
				<form class="login100-form validate-form" method="post" enctype="multipart/form-data">
					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100">Tài khoản</span>
						<input class="input100" type="text" name="username" placeholder="Enter username">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-26" data-validate="name is required">
						<span class="label-input100">Họ tên</span>
						<input class="input100" type="text" name="name" placeholder="Enter name">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate="Password is required">
						<span class="label-input100">Mật khẩu</span>
						<input class="input100" type="password" name="pass" placeholder="Enter password">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-18" data-validate="Password is required">
						<span class="label-input100">Nhập lại mật khẩu</span>
						<input class="input100" type="password" name="rppass" placeholder="Enter password">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100  m-b-26" data-validate="images is required">
						<span class="label-input100">Ảnh</span>
						<input class="input100" type="file" name="images" placeholder="Enter images">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-26" data-validate="address is required">
						<span class="label-input100">Địa chỉ</span>
						<input class="input100" type="text" name="address" placeholder="Enter address">
						<span class="focus-input100"></span>
					</div>
					<div class="flex-sb-m w-full p-b-30">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>

						<div>
							<a href="#" class="txt1">
								Forgot Password?
							</a>
						</div>

					</div>
					<div class="container-login100-form-btn">
						<input onclick="dangki()" name="gui" type="submit" value="Đăng kí" class="login100-form-btn" style="cursor:pointer">
						<a href="../index.php" style="margin-left: 50px; "><input style=" background-color: red; cursor:pointer" type="button" value="Đăng nhập" class="login100-form-btn"></a>

					</div>
				</form>

			</div>
		</div>

	</div>

	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>

</html>
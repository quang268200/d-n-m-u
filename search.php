<?php session_start() ?>
<?php ob_start() ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="./css/bootstrap.min.css">
  <script src="./js/jquery.js"></script>
  <script src='./js/fontawesome.js'></script>
  <script src="./js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="./css/index.css">
</head>

<body>
  <?php
  include './include/header.php';
  include './admin/connect.php';
  ?>

  <div class="main">
    <div class="row">
      <div class="main-left col-md-9">
        <?php
        if (isset($_GET["keyword"])) {
          $keyword = $_GET["keyword"];
          $sql_keyword = "SELECT * FROM products where name_product like '%$keyword%'";
          $kq_keyword = $conn->query($sql_keyword);
          echo    '<div class="sanpham">
          <p>Kết quả tìm kiếm cho " ' . $keyword. '"</p>
        </div>';
          //lấy trang hiện tại
          $limit = 8;
          if (isset($_GET["page"])) {
            $current_page = $_GET["page"];
          } else {
            $current_page = 1;
          }
          $start = ($current_page - 1) * $limit;
          //đếm tổng sản phẩm có trong danh mục
          $sql_count_products = "SELECT count(id_product) FROM products where name_product like '%$keyword%'";
          $kq_count = $conn->query($sql_count_products)->fetchColumn();
          // echo $kq_count;
          //tổng số trang
          $total_page = ceil($kq_count / $limit);
          //  echo $total_page;
          //lấy sản phẩm thuộc danh mục
          $sql_products = "SELECT * FROM products where name_product like '%$keyword%' LIMIT $start,$limit";
          $kq_products = $conn->query($sql_products);
        }
        ?>
        <ul>
          <?php
          // if ($kq_products->fetchColumn()==0) {
          //   echo "Chưa có sản phẩm";
          // } else {
            ?>
            <?php
              foreach ($kq_products as $key => $value) {
                ?>
              <li>
                <a href="chitietsp.php?id=<?php echo $value["id_product"] ?>">
                  <div class="anhsp">
                    <img src="./admin/images/<?php echo $value["images"] ?>" alt="" width="170px" height="170px">
                  </div>
                  <h5><?php echo $value['name_product'] ?></h5>
                  <p><?php echo number_format($value['sale']) ?> đ</p>
                  <span><del><?php echo number_format($value['price']) ?> đ</del></span>
                </a>
              </li>
            <?php
              }
              ?>
          <?php
          // }
          ?>

        </ul>
        <div class="clear"></div>
        <div class="phantrang">
          <ul class="pagination ">
            <?php
            if ($total_page > 1) {
              if ($current_page > 1) {
                echo '<li class="page-item"><a class="page-link" href="search.php?keyword=' . $keyword . '&&page=' . ($current_page - 1) . '">Previous</a></li>';
              }
              for ($i = 1; $i <= $total_page; $i++) {
                if ($current_page == $i) {
                  echo '<li class="page-item active"><a class="page-link " href="">' . $i . '</a></li>';
                } else {
                  echo '<li class="page-item"><a class="page-link" href="search.php?keyword=' . $keyword . '&&page=' . $i . '">' . $i . '</a></li>';
                }
              }
              if ($current_page < $total_page) {
                echo '<li class="page-item"><a class="page-link" href="search.php?keyword=' . $keyword . '&&page=' . ($current_page + 1) . '">next</a></li>';
              }
            }
            ?>

          </ul>
        </div>
      </div>
      <?php include './include/slidebar.php' ?>
    </div>
  </div>
  <?php include './include/footer.php' ?>
</body>

</html>